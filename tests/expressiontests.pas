unit ExpressionTests;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  fpcunit,
  testutils,
  testregistry,
  cnocExpressionEvaluator;

type

  { TTestExpressions }

  TTestExpressions = class(TTestCase)
  private
    FIdentifierList: TEvaluationIdentifierList;
  protected
    procedure ProvideIdentifier(const AnIdentifier: TEvaluationIdentifier);
    procedure ProvideIdentifier(const AName: string; AnEvaluationValue: TEvaluationValue);
    procedure FillIdentifiers(AnEvaluator: TExpressionEvaluator);

    function IntTestExpression(const AnExpression: string; const AnExpectedKind: TEvaluationValueKind): TEvaluationValue;
    procedure IntTestEvaluationError(const AnExpression, AnExpectedErrorMessage: string);
    procedure IntTestExpressionIntegerResult(const AnExpression: string; AnExpectedResult: int64);
    procedure IntTestExpressionStringResult(const AnExpression: string; AnExpectedResult: string);
    procedure IntTestExpressionBooleanResult(const AnExpression: string; AnExpectedResult: Boolean);
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestBasicExpressions;
    procedure TestBasicExpressionsWithIdentifiers;
    procedure TestComparisionExpressions;
  end;

implementation

procedure TTestExpressions.TestBasicExpressions;
begin
  IntTestExpressionIntegerResult('1+1', 2);
  IntTestExpressionIntegerResult('123-51', 72);
  IntTestExpressionIntegerResult('3-5', -2);
  IntTestExpressionIntegerResult('3*5', 15);
  IntTestExpressionIntegerResult('9 div 2', 4);
  IntTestExpressionIntegerResult('3 * -5', -15);
  IntTestExpressionIntegerResult('(3+1)*5', 20);
  IntTestExpressionStringResult('''hello'' + '' '' + ''world''', 'hello world');
  IntTestExpressionStringResult('''It''''s'' + '' perfect''', 'It''s perfect');

  IntTestExpressionBooleanResult('true', True);
  IntTestExpressionBooleanResult('True', True);

  IntTestExpressionBooleanResult('false', False);
  IntTestExpressionBooleanResult('FALSE', False);

  IntTestExpressionBooleanResult('not true', False);
  IntTestExpressionBooleanResult('not false', True);
end;

function TTestExpressions.IntTestExpression(const AnExpression: string; const AnExpectedKind: TEvaluationValueKind): TEvaluationValue;
var
  Evaluator: TExpressionEvaluator;
  Value: TEvaluationValue;
begin
  Evaluator := TExpressionEvaluator.Create(AnExpression);
  try
    Check(Evaluator.Parse, Format('Failed to parse expression [%s]: %s', [AnExpression, Evaluator.Error]));
    FillIdentifiers(Evaluator);
    Value := Evaluator.Evaluate;
    try
      Check(Assigned(Value), Format('Failed to evaluate expression [%s]: %s', [AnExpression, Evaluator.Error]));
      Check(AnExpectedKind = Value.Kind, Format('The resultkind is not what is expected for expression [%s]', [AnExpression]));
      Result := Value;
      Value := nil;
    finally
      Value.Free;
    end;
  finally
    Evaluator.Free;
  end;
end;

procedure TTestExpressions.IntTestExpressionIntegerResult(const AnExpression: string; AnExpectedResult: int64);
var
  Value: TEvaluationValue;
  ErrMessage: string;
  Int: Int64;
begin
  Value := IntTestExpression(AnExpression, evkInteger);
  try
    Check(Value.TryGetAsInteger(Int, ErrMessage), Format('Failed to convert evaluation-result of expression [%s] to integer: [%s]', [AnExpression, ErrMessage]));
    CheckEquals(AnExpectedResult, Int, Format('Result of expression [%s]', [AnExpression]));
  finally
    Value.Free;
  end;
end;

procedure TTestExpressions.TestBasicExpressionsWithIdentifiers;
begin
  IntTestEvaluationError('i', 'No value provided for identifier [i]');

  ProvideIdentifier('i', TIntegerEvaluationValue.Create(65));
  IntTestExpressionIntegerResult('i', 65);
  IntTestExpressionIntegerResult('i+5', 70);
  IntTestExpressionIntegerResult('i+I', 130);
end;

procedure TTestExpressions.IntTestEvaluationError(const AnExpression, AnExpectedErrorMessage: string);
var
  Evaluator: TExpressionEvaluator;
  Value: TEvaluationValue;
begin
  Evaluator := TExpressionEvaluator.Create(AnExpression);
  try
    Check(Evaluator.Parse, Format('Failed to parse expression [%s]: %s', [AnExpression, Evaluator.Error]));
    FillIdentifiers(Evaluator);
    Value := Evaluator.Evaluate;
    try
      Check(not Assigned(Value), Format('The evaluation provided a result, while an error was expected for expression [%s]', [AnExpression]));
      CheckEquals(AnExpectedErrorMessage, Evaluator.Error, Format('Expected error message for expression [%s]', [AnExpression]));
    finally
      Value.Free;
    end;
  finally
    Evaluator.Free;
  end;
end;

procedure TTestExpressions.ProvideIdentifier(const AName: string; AnEvaluationValue: TEvaluationValue);
var
  Identifier: TEvaluationIdentifier;
begin
  Identifier := TEvaluationIdentifier.Create(AName);
  Identifier.Value := AnEvaluationValue;
  ProvideIdentifier(Identifier);
end;

procedure TTestExpressions.ProvideIdentifier(const AnIdentifier: TEvaluationIdentifier);
begin
  FIdentifierList.Add(AnIdentifier);
end;

procedure TTestExpressions.SetUp;
begin
  inherited SetUp;
  FIdentifierList := TEvaluationIdentifierList.Create;
end;

procedure TTestExpressions.TearDown;
begin
  FIdentifierList.Free;
  inherited TearDown;
end;

procedure TTestExpressions.FillIdentifiers(AnEvaluator: TExpressionEvaluator);
var
  i: Integer;
  DestIdentifier: TEvaluationIdentifier;
  SourceIdentifier: TEvaluationIdentifier;
begin
  for i := 0 to AnEvaluator.IdentifierList.Count -1 do
    begin
    DestIdentifier := AnEvaluator.IdentifierList[i];
    SourceIdentifier := FIdentifierList.FindByName(DestIdentifier.Name);
    if Assigned(SourceIdentifier) then
      DestIdentifier.Value := SourceIdentifier.Value.Clone;
    end;
end;

procedure TTestExpressions.IntTestExpressionStringResult(const AnExpression: string; AnExpectedResult: string);
var
  Value: TEvaluationValue;
  ErrMessage: string;
  Str: string;
begin
  Value := IntTestExpression(AnExpression, evkString);
  try
    Check(Value.TryGetAsString(Str, ErrMessage), Format('Failed to convert evaluation-result of expression [%s] to string: [%s]', [AnExpression, ErrMessage]));
    CheckEquals(AnExpectedResult, Str, Format('Result of expression [%s]', [AnExpression]));
  finally
    Value.Free;
  end;
end;

procedure TTestExpressions.TestComparisionExpressions;
begin
  IntTestExpressionBooleanResult('1=1', True);
  IntTestExpressionBooleanResult('1=2', False);

  IntTestExpressionBooleanResult('1<>2', True);
  IntTestExpressionBooleanResult('1<>1', False);

  IntTestExpressionBooleanResult('1<2', True);
  IntTestExpressionBooleanResult('1<1', False);

  IntTestExpressionBooleanResult('1>2', False);
  IntTestExpressionBooleanResult('2>1', True);

  IntTestExpressionBooleanResult('''hoi''=''hoi''', True);
  IntTestExpressionBooleanResult('''hoi''=''dag''', False);

  IntTestExpressionBooleanResult('''hoi''<>''hoi''', False);
  IntTestExpressionBooleanResult('''hoi''<>''dag''', True);

  IntTestExpressionBooleanResult('''a''<''b''', True);
  IntTestExpressionBooleanResult('''B''>''A''', True);

end;

procedure TTestExpressions.IntTestExpressionBooleanResult(const AnExpression: string; AnExpectedResult: Boolean);
var
  Value: TEvaluationValue;
  ErrMessage: string;
  Bool: Boolean;
begin
  Value := IntTestExpression(AnExpression, evkBoolean);
  try
    Check(Value.TryGetAsBoolean(Bool, ErrMessage), Format('Failed to convert evaluation-result of expression [%s] to boolean: [%s]', [AnExpression, ErrMessage]));
    CheckEquals(AnExpectedResult, Bool, Format('Result of expression [%s]', [AnExpression]));
  finally
    Value.Free;
  end;
end;

initialization
  RegisterTest(TTestExpressions);
end.

