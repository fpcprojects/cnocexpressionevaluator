# Expression-evaluator

This libraries provide the tools to evaluate Pascal expressions, using fcl-passrc to parse those expressions.

Expressions like '1+1' can be evaluated, but also complex expressions and expressions that contain identifiers, like '(i+3)*x'. Values for these identifiers can be provided by the application.

## Getting started

### Prerequisites

This library needs at least FreePascal version 3.3.1 and fcl-passrc. To install it easily, the package-manager fppkg is used.

### Installing

Installation can be done from the central fppkg-repository:

    fppkg install cnocExpressionEvaluator

It is also possible to download the sources, start a command-line and make the source-directory the current-directory. Then type:

    fppkg install

Now all units from the cnocExpressionEvaluator-package can be used in your Free Pascal applications.

### Basic usage

An expression is evaluated using the `TExpressionEvaluatorobject`. First the expression has to be parsed, then it can be evaluated. The result is returned as a `TEvaluationValue`, which has to be freed after use. See this example:

```
program EvaluatorExample1;

{$mode objfpc}

uses
  cnocExpressionEvaluator;

var
  Evaluator: TExpressionEvaluator;
  Value: TEvaluationValue;
begin
  // Evaluate the expression 1+1.
  Evaluator := TExpressionEvaluator.Create('1+1');
  try
    if not Evaluator.Parse then
      begin
      Writeln('Parse error: ', Evaluator.Error);
      Halt(1);
      end
    else
      begin
      Value := Evaluator.Evaluate;
      try
        if not Assigned(Value) then
          begin
          Writeln('Evaluation error: ', Evaluator.Error);
          Halt(2);
          end
        else
          // The result is 2
          Writeln('Evaluation result: ', Value.AsExpressionString);
      finally
        Value.Free;
      end;
      end;
  finally
    Evaluator.Free;
  end;
end.
```

### Using identifiers

All identifiers found in the expression during parsing, are added to a list. Before the expression is evaluated,
a value for those identifiers has to be provided, or else the evaluation will fail. An value has to be provided in the
form of a `TEvaluationValue`. This could look like this:

```
  Evaluator.IdentifierList.FindByName('i').Value := TIntegerEvaluationValue.Create(5);
```

See for some full examples the tests.

## Tests and example

There is one Lazarus-example included. The example is simple form in which an expression can be entered, which will
be evaluated.

![Screenshots with some expressions](example/EvaluatorScreenshots.png)

The tests are in a fpcunit-test project in the tests directory.

## Known issues

This project is not finished yet. These are a few things that do not work as desired. (Patches are always welcome)

 - Evaluating float's
 - Evaluating functions
 - 'and'/'or' statements and such
 - Parsing (fcl-passrc) error-handling

## Contributing

Questions, ideas and patches are welcome. You can send them directly to <joost@cnoc.nl>, or discuss them at the fpc-pascal mailinglist.

## Versioning

This package is very new and receives a lot of updates. For the versions available, see the tags on this repository.

## Authors

* **Joost van der Sluis** - *initial implementation* - <joost@cnoc.nl>

## License

This library is distributed under the Library GNU General Public License version 2 (see the [COPYING.LGPL.txt](COPYING.LGPL.txt) file) with the following modification:

- object files and libraries linked into an application may be distributed without source code. As further eplained in the file [COPYING.modifiedLGPL.txt](COPYING.modifiedLGPL.txt).