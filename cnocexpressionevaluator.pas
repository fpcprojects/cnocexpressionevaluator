unit cnocExpressionEvaluator;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Generics.Collections,
  Contnrs,
  PParser,
  PScanner,
  PasTree;

type
  TEvaluationValueKind = (
    evkNone,
    evkString,
    evkBoolean,
    evkInteger,
    evkCardinal,
    evkFloat,
    evkFunction,
    evkContainer,
    evkType);
  TEvaluationValue = class;
  TEvaluationValueArray = array of TEvaluationValue;

  { TEvaluationValue }

  TEvaluationValue = class
  private
  protected
    FKind: TEvaluationValueKind;
    function GetAsExpressionString: string; virtual;
  public
    function TryGetAsString(out Str: string; out ErrMessage: string): Boolean; virtual;
    function TryGetAsInteger(out Int: Int64; out ErrMessage: string): Boolean; virtual;
    function TryGetAsCardinal(out Cardinal: QWord; out ErrMessage: string): Boolean; virtual;
    function TryGetAsFloat(out Float: ValReal; out ErrMessage: string): Boolean; virtual;
    function TryGetAsBoolean(out Bool: Boolean; out ErrMessage: string): Boolean; virtual;
    function TryObtainChild(ChildName: string; out EvaluationValue: TEvaluationValue; out ErrMessage: string): Boolean; virtual;
    function Clone: TEvaluationValue; virtual;
    property Kind: TEvaluationValueKind read FKind;
    property AsExpressionString: string read GetAsExpressionString;
  end;

  { TIntegerEvaluationValue }

  TIntegerEvaluationValue = class(TEvaluationValue)
  protected
    FValue: Int64;
    function GetAsExpressionString: string; override;
  public
    constructor Create(AValue: Int64);
    function Clone: TEvaluationValue; override;
    function TryGetAsInteger(out Int: Int64; out ErrMessage: string): Boolean; override;
  end;

  { TStringEvaluationValue }

  TStringEvaluationValue = class(TEvaluationValue)
  protected
    FValue: string;
    function GetAsExpressionString: string; override;
  public
    constructor Create(AValue: string);
    function Clone: TEvaluationValue; override;
    function TryGetAsString(out Str: string; out ErrMessage: string): Boolean; override;
  end;

  { TBooleanEvaluationValue }

  TBooleanEvaluationValue = class(TEvaluationValue)
  protected
    FValue: Boolean;
    function GetAsExpressionString: string; override;
  public
    constructor Create(AValue: Boolean);
    function Clone: TEvaluationValue; override;
    function TryGetAsBoolean(out Bool: Boolean; out ErrMessage: string): Boolean; override;
  end;

  { TCardinalEvaluationValue }

  TCardinalEvaluationValue = class(TEvaluationValue)
  protected
    FValue: QWord;
    function GetAsExpressionString: string; override;
  public
    constructor Create(AValue: QWord);
    function Clone: TEvaluationValue; override;
    function TryGetAsCardinal(out Cardinal: QWord; out ErrMessage: string): Boolean; override;
    function TryGetAsInteger(out Int: Int64; out ErrMessage: string): Boolean; override;
    function TryGetAsFloat(out Float: ValReal; out ErrMessage: string): Boolean; override;
  end;

  { TFloatEvaluationValue }

  TFloatEvaluationValue = class(TEvaluationValue)
  protected
    FValue: ValReal;
    function GetAsExpressionString: string; override;
  public
    constructor Create(AValue: ValReal);
    function Clone: TEvaluationValue; override;
    function TryGetAsFloat(out Float: ValReal; out ErrMessage: string): Boolean; override;
  end;

  { TEvaluationIdentifier }

  TEvaluationIdentifier = class;
  TCallParamsFuncProc = function(const Func: TEvaluationValue; const ParamValues: array of TEvaluationValue; out ErrMessage: string): TEvaluationValue of object;
  TGetValueProc = function(const Sender: TEvaluationIdentifier; out ErrMessage: string): TEvaluationValue of object;
  TGetIndexedValueProc = function(const Value: TEvaluationValue; IndexValues: TEvaluationValueArray; out ErrMessage: string): TEvaluationValue of object;
  TTypecastProc = function(const Value: TEvaluationValue; const TypeDefinition: TEvaluationValue; out ErrMessage: string): TEvaluationValue of object;

  TEvaluationIdentifier = class
  private
    FName: string;
    FValue: TEvaluationValue;
    FOnCallParamsFunc: TCallParamsFuncProc;
    FOnGetValue: TGetValueProc;
    FOnGetIndexedValue: TGetIndexedValueProc;
    procedure SetValue(AValue: TEvaluationValue);
  public
    constructor Create(AName: string);
    destructor Destroy; override;
    function IndexedValue(const IndexValues: TEvaluationValueArray): TEvaluationValue;
    property Name: string read FName write FName;
    property Value: TEvaluationValue read FValue write SetValue;
    property OnCallParamsFunc: TCallParamsFuncProc read FOnCallParamsFunc write FOnCallParamsFunc;
    property OnGetValue: TGetValueProc read FOnGetValue write FOnGetValue;
    property OnGetIndexedValue: TGetIndexedValueProc read FOnGetIndexedValue write FOnGetIndexedValue;
  end;

  TGenEvaluationIdentifierList = specialize TObjectList<TEvaluationIdentifier>;

  { TEvaluationIdentifierList }

  TEvaluationIdentifierList = class(TGenEvaluationIdentifierList)
  public
    function FindByName(AName: string): TEvaluationIdentifier;
  end;

  { TExpressionEvaluator }

  TExpressionEvaluator = class
  private
    FEngine: TPasTreeContainer;
    FExpression: string;
    FOnTypecast: TTypecastProc;
    FPasExpression: TPasExpr;
    FError: string;
    FOnCallParamsFunc: TCallParamsFuncProc;
    FOnGetIndexedValue: TGetIndexedValueProc;
  protected
    FIdentifierList: TEvaluationIdentifierList;

    function ExtractIdentifiers(AnExpression: TPasExpr): Boolean;

    function EvaluateExpr(AnExpression: TPasExpr): TEvaluationValue;
    function TryEvaluateBinaryArithmeticOperator(const ALeftValue, ARightValue: TEvaluationValue; const OpCode: TExprOpCode): TEvaluationValue;
    function TryEvaluateBinaryLogicalOperator(const ALeftValue, ARightValue: TEvaluationValue; const OpCode: TExprOpCode): TEvaluationValue;
    function TryEvaluateUnaryOperator(const AnOperand: TEvaluationValue; const OpCode: TExprOpCode): TEvaluationValue;
    function TryEvaluateSubIdentOperator(const ALeftValue: TEvaluationValue; const ARightExpression: TPasExpr): TEvaluationValue;

    procedure SetError(AnErrorMsg: string); overload;
    procedure SetError(Const AnErrorMsg: string; const Args: array of const); overload;
  public
    constructor Create(AnExpression: string);
    destructor Destroy; override;
    function Parse: Boolean;
    function Evaluate: TEvaluationValue;
    property OnCallParamsFunc: TCallParamsFuncProc read FOnCallParamsFunc write FOnCallParamsFunc;
    property OnTypecast: TTypecastProc read FOnTypecast write FOnTypecast;
    property OnGetIndexedValue: TGetIndexedValueProc read FOnGetIndexedValue write FOnGetIndexedValue;
    property IdentifierList: TEvaluationIdentifierList read FIdentifierList;
    property Error: string read FError;
  end;

implementation

type
  { TSimpleEngine }

  { We have to override abstract TPasTreeContainer methods.
    See utils/fpdoc/dglobals.pp for an implementation of TFPDocEngine,
    a "real" engine. }

  TSimpleEngine = class(TPasTreeContainer)
  public
    function CreateElement(
      AClass: TPTreeElement;
      const AName: String;
      AParent: TPasElement;
      AVisibility: TPasMemberVisibility;
      const ASourceFilename: String;
      ASourceLinenumber: Integer): TPasElement; override;
    function FindElement(const AName: String): TPasElement; override;
  end;

{$if (FPC_FULLVERSION <= 30202)}
  { TPasParser }

  TPasParser = class(PParser.TPasParser)
  public
    procedure ParseAdhocExpression(out NewExprElement: TPasExpr);
  end;

{ TPasParser }

procedure TPasParser.ParseAdhocExpression(out NewExprElement: TPasExpr);
begin
  NewExprElement := DoParseExpression(nil);
end;
{$endif}

{ TFloatEvaluationValue }

constructor TFloatEvaluationValue.Create(AValue: ValReal);
begin
  FKind := evkFloat;
  FValue := AValue;
end;

function TFloatEvaluationValue.Clone: TEvaluationValue;
begin
  Result := TFloatEvaluationValue.Create(FValue);
end;

function TFloatEvaluationValue.TryGetAsFloat(out Float: ValReal; out ErrMessage: string): Boolean;
begin
  Result := True;
  Float := FValue;
  ErrMessage := '';
end;

function TFloatEvaluationValue.GetAsExpressionString: string;
begin
  Result := FloatToStr(FValue);
end;

{ TCardinalEvaluationValue }

constructor TCardinalEvaluationValue.Create(AValue: QWord);
begin
  FKind := evkCardinal;
end;

function TCardinalEvaluationValue.Clone: TEvaluationValue;
begin
  Result := TCardinalEvaluationValue.Create(FValue);
end;

function TCardinalEvaluationValue.TryGetAsCardinal(out Cardinal: QWord; out ErrMessage: string): Boolean;
begin
  Result := True;
  ErrMessage := '';
  Cardinal := FValue;
end;

function TCardinalEvaluationValue.TryGetAsInteger(out Int: Int64; out ErrMessage: string): Boolean;
begin
  if FValue < High(Int64) then
    begin
    Result := True;
    ErrMessage := '';
    Int := FValue;
    end
  else
    begin
    Result := False;
    ErrMessage := 'Range check error';
    Int := 0;
    end;
end;

function TCardinalEvaluationValue.TryGetAsFloat(out Float: ValReal; out ErrMessage: string): Boolean;
begin
  Result := True;
  ErrMessage := '';
  Float := FValue;
end;

function TCardinalEvaluationValue.GetAsExpressionString: string;
begin
  Result := IntToStr(FValue);
end;

{ TBooleanEvaluationValue }

constructor TBooleanEvaluationValue.Create(AValue: Boolean);
begin
  FKind := evkBoolean;
  FValue := AValue;
end;

function TBooleanEvaluationValue.Clone: TEvaluationValue;
begin
  Result := TBooleanEvaluationValue.Create(FValue);
end;

function TBooleanEvaluationValue.TryGetAsBoolean(out Bool: Boolean; out ErrMessage: string): Boolean;
begin
  ErrMessage := '';
  Result := True;
  Bool := FValue;
end;

function TBooleanEvaluationValue.GetAsExpressionString: string;
begin
  if FValue then
    Result := 'True'
  else
    Result := 'False'
end;

{ TStringEvaluationValue }

constructor TStringEvaluationValue.Create(AValue: string);
begin
  FKind := evkString;
  FValue := AValue;
end;

function TStringEvaluationValue.Clone: TEvaluationValue;
begin
  Result := TStringEvaluationValue.Create(FValue);
end;

function TStringEvaluationValue.TryGetAsString(out Str: string; out ErrMessage: string): Boolean;
begin
  ErrMessage := '';
  Result := True;
  Str := FValue;
end;

function TStringEvaluationValue.GetAsExpressionString: string;
begin
  Result := AnsiQuotedStr(FValue, '''');
end;

{ TEvaluationIdentifierList }

function TEvaluationIdentifierList.FindByName(AName: string): TEvaluationIdentifier;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count -1 do
    if SameText(Items[i].Name, AName) then
      begin
      Result := Items[i];
      Exit;
      end;
end;

{ TEvaluationIdentifier }

constructor TEvaluationIdentifier.Create(AName: string);
begin
  FName := AName;
end;

destructor TEvaluationIdentifier.Destroy;
begin
  FValue.Free;
  inherited Destroy;
end;

procedure TEvaluationIdentifier.SetValue(AValue: TEvaluationValue);
begin
  if Assigned(FValue) then
    raise Exception.Create('Value already assigned');
  FValue := AValue;
end;

function TEvaluationIdentifier.IndexedValue(const IndexValues: TEvaluationValueArray): TEvaluationValue;
begin
  Result := nil;
end;

{ TIntegerEvaluationValue }

constructor TIntegerEvaluationValue.Create(AValue: Int64);
begin
  FKind := evkInteger;
  FValue := AValue;
end;

function TIntegerEvaluationValue.TryGetAsInteger(out Int: Int64; out ErrMessage: string): Boolean;
begin
  ErrMessage := '';
  Int := FValue;
  Result := True;
end;

function TIntegerEvaluationValue.Clone: TEvaluationValue;
begin
  Result := TIntegerEvaluationValue.Create(FValue);
end;

function TIntegerEvaluationValue.GetAsExpressionString: string;
begin
  Result := IntToStr(FValue);
end;

{ TEvaluationValue }

function TEvaluationValue.TryGetAsString(out Str: string; out ErrMessage: string): Boolean;
begin
  Str := '';
  Result := False;
  ErrMessage := 'Unable to convert value to string';
end;

function TEvaluationValue.TryGetAsInteger(out Int: Int64; out ErrMessage: string): Boolean;
begin
  Int := 0;
  Result := False;
  ErrMessage := 'Unable to convert value to integer';
end;

function TEvaluationValue.Clone: TEvaluationValue;
begin
  Result := TEvaluationValue.Create;
  Result.FKind := FKind;
end;

function TEvaluationValue.TryGetAsBoolean(out Bool: Boolean; out ErrMessage: string): Boolean;
begin
  Bool := False;
  Result := False;
  ErrMessage := 'Unable to convert value to boolean';
end;

function TEvaluationValue.TryGetAsCardinal(out Cardinal: QWord; out ErrMessage: string): Boolean;
begin
  Cardinal := 0;
  Result := False;
  ErrMessage := 'Unable to convert value to cardinal';
end;

function TEvaluationValue.TryGetAsFloat(out Float: ValReal; out ErrMessage: string): Boolean;
begin
  Float := 0;
  Result := False;
  ErrMessage := 'Unable to convert value to float';
end;

function TEvaluationValue.GetAsExpressionString: string;
begin
  Result := '';
end;

function TEvaluationValue.TryObtainChild(ChildName: string; out EvaluationValue: TEvaluationValue; out ErrMessage: string): Boolean;
begin
  EvaluationValue := nil;
  Result := False;
  ErrMessage := 'No child named ' + ChildName;
end;

function TSimpleEngine.CreateElement(AClass: TPTreeElement; const AName: string;
  AParent: TPasElement; AVisibility: TPasMemberVisibility; const ASourceFilename: string;
  ASourceLinenumber: Integer): TPasElement;
begin
  Result := AClass.Create(AName, AParent);
  Result.Visibility := AVisibility;
  Result.SourceFilename := ASourceFilename;
  Result.SourceLinenumber := ASourceLinenumber;
end;

function TSimpleEngine.FindElement(const AName: string): TPasElement;
begin
  { dummy implementation, see TFPDocEngine.FindElement for a real example }
  Result := nil;
end;

{ TExpressionEvaluator }

constructor TExpressionEvaluator.Create(AnExpression: string);
begin
  FEngine := TSimpleEngine.Create;
  FExpression := AnExpression;
  FIdentifierList := TEvaluationIdentifierList.Create;
end;

destructor TExpressionEvaluator.Destroy;
begin
  FIdentifierList.Free;
  if Assigned(FPasExpression) then
    FPasExpression.Release;
  FEngine.Free;
  inherited Destroy;
end;

function TExpressionEvaluator.Evaluate: TEvaluationValue;
begin
  Result := EvaluateExpr(FPasExpression);
end;

function TExpressionEvaluator.Parse: Boolean;
var
  FileResolver: TStreamResolver;
  Scanner: TPascalScanner;
  Parser: TPasParser;
begin
  Result := False;
  if Assigned(FPasExpression) then
    raise Exception.Create('TcnocExpressionEvaluator.Parse can not be called twice');

  FileResolver := nil;
  Scanner := nil;
  Parser := nil;
  try
    FileResolver := TStreamResolver.Create;
    FileResolver.OwnsStreams:=true;

    FileResolver.AddStream('Expression', TStringStream.Create(FExpression));

    Scanner := TPascalScanner.Create(FileResolver);
    Scanner.LogEvents:=FEngine.ScannerLogEvents;
    Scanner.OnLog:=FEngine.Onlog;

    Parser := TPasParser.Create(Scanner, FileResolver, FEngine);
    Parser.LogEvents:=FEngine.ParserLogEvents;
    Parser.OnLog:=FEngine.Onlog;

    Scanner.OpenFile('Expression');

    parser.NextToken;
    parser.ParseAdhocExpression(FPasExpression);

    Result := ExtractIdentifiers(FPasExpression);
  finally
    Parser.Free;
    Scanner.Free;
    FileResolver.Free;
  end;
end;


function TExpressionEvaluator.EvaluateExpr(AnExpression: TPasExpr): TEvaluationValue;

  function EvaluateIdentifier(AName: string): TEvaluationValue;
  var
    EVal: TEvaluationValue;
    Identifier: TEvaluationIdentifier;
  begin
    Identifier := FIdentifierList.FindByName(AName);
    if not Assigned(Identifier) then
      // May never happen
      raise Exception.CreateFmt('Evaluation problem, identifier [%s] not found.', [AName]);
    if Assigned(Identifier.OnGetValue) then
      Result := Identifier.OnGetValue(Identifier, FError)
    else
      begin
      EVal := Identifier.Value;
      if not Assigned(EVal) then
        begin
        SetError('No value provided for identifier [%s]', [AName]);
        Result := nil;
        end
      else
        Result := EVal.Clone;
      end;
  end;

var
  LeftExpr, RightExpr, FuncValue: TEvaluationValue;
  ParamsExpr: TParamsExpr;
  ParamValues: TEvaluationValueArray;
  i: Integer;
  Eval: TEvaluationValue;
  ErrMessage: string;

begin
  Result := nil;
  RightExpr := nil;
  if not assigned(AnExpression) then
    begin
    SetError('Incomplete expression');
    Exit;
    end;
  case AnExpression.Kind of
    pekBoolConst:
      Result := TBooleanEvaluationValue.Create((AnExpression as TBoolConstExpr).Value);
    pekNumber:
      Result := TIntegerEvaluationValue.Create(StrToInt64((AnExpression as TPrimitiveExpr).Value));
    pekString:
      Result := TStringEvaluationValue.Create(AnsiDequotedStr((AnExpression as TPrimitiveExpr).Value, ''''));
    pekBinary:
      begin
      LeftExpr := EvaluateExpr(TBinaryExpr(AnExpression).left);
      try
        if not Assigned(LeftExpr) then
          Exit;
        if AnExpression.OpCode = eopSubIdent then
          begin
          Result := TryEvaluateSubIdentOperator(LeftExpr, TBinaryExpr(AnExpression).right);
          Exit;
          end;
        RightExpr := EvaluateExpr(TBinaryExpr(AnExpression).right);
        try
          if not Assigned(RightExpr) then
            Exit;
          case AnExpression.OpCode of
            eopAdd,
            eopSubtract,
            eopMultiply,
            eopDivide,
            eopDiv,
            eopMod,
            eopPower:
              Result := TryEvaluateBinaryArithmeticOperator(LeftExpr, RightExpr, AnExpression.OpCode);
            eopEqual,
            eopNotEqual,
            eopLessThan,
            eopGreaterThan,
            eopLessthanEqual,
            eopGreaterThanEqual:
              Result := TryEvaluateBinaryLogicalOperator(LeftExpr, RightExpr, AnExpression.OpCode);
          else
            SetError('Unsupported opcode');
          end;
        finally
          RightExpr.Free;
        end;
      finally
        LeftExpr.Free;
      end;
      end;
      //
      //  //eopEqual:    result := EvaluateExpr(ASession, TBinaryExpr(AnExpression).left) = EvaluateExpr(ASession, TBinaryExpr(AnExpression).right);
      //  //eopNotEqual: result := EvaluateExpr(ASession, TBinaryExpr(AnExpression).left) <> EvaluateExpr(ASession, TBinaryExpr(AnExpression).right);
      //  //eopOr:       result := EvaluateExpr(ASession, TBinaryExpr(AnExpression).left) or EvaluateExpr(ASession, TBinaryExpr(AnExpression).right);
      //  //eopAnd:      result := EvaluateExpr(ASession, TBinaryExpr(AnExpression).left) and EvaluateExpr(ASession, TBinaryExpr(AnExpression).right);
      //  //eopGreaterThan: result := EvaluateExpr(ASession, TBinaryExpr(AnExpression).left) > EvaluateExpr(ASession, TBinaryExpr(AnExpression).right);
      //  //eopLessThan: result := EvaluateExpr(ASession, TBinaryExpr(AnExpression).left) < EvaluateExpr(ASession, TBinaryExpr(AnExpression).right);
      //else
      //  raise exception.CreateFmt('Unsupported opcode (%s) in expression', [OpcodeStrings[AnExpression.OpCode]]);
      //end;
      //end;
    pekUnary:
      begin
      LeftExpr := EvaluateExpr(TUnaryExpr(AnExpression).Operand);
      try
        if not Assigned(LeftExpr) then
          Exit;
        Result := TryEvaluateUnaryOperator(LeftExpr, AnExpression.OpCode);
      finally
        LeftExpr.Free;
      end;
      end;
    pekIdent:
      begin
      Eval := EvaluateIdentifier((AnExpression as TPrimitiveExpr).Value);
      if Assigned(Eval) and (Eval.Kind=evkFunction) then
        begin
        // I am not happy with this solution, it feels like some kind of magic
        // to call the function here all of a sudden. But I do not see a better
        // solution.
        try
          Result := OnCallParamsFunc(Eval, [], ErrMessage);
          if not Assigned(Result) then
            SetError(ErrMessage);
        finally
          Eval.Free;
        end;
        end
      else
        Result := Eval;
      end;
    pekFuncParams,
    pekArrayParams:
      begin
      ParamsExpr := AnExpression as TParamsExpr;
      if ParamsExpr.Value.Kind = pekIdent then
        FuncValue := EvaluateIdentifier((ParamsExpr.Value as TPrimitiveExpr).Value)
      else
        FuncValue := EvaluateExpr(ParamsExpr.Value);

      try
        SetLength(ParamValues, Length(ParamsExpr.Params));
        try
          for i := 0 to High(ParamsExpr.Params) do
            begin
            ParamValues[i] := EvaluateExpr(ParamsExpr.Params[i]);
            if not Assigned(ParamValues[i]) then
              Exit;
            end;

          if (AnExpression.Kind=pekFuncParams) or (FuncValue.Kind=evkFunction) then
            begin
            if (FuncValue.Kind=evkType) and (Length(ParamValues)=1) then
              begin
              // Diffrentiate between function-calls and typecasts.
              if not Assigned(OnTypecast) then
                begin
                SetError('Typecasts are not supported');
                Exit;
                end;
              Result := OnTypecast(ParamValues[0], FuncValue, FError);
              end
            else
              begin
              if not Assigned(OnCallParamsFunc) then
                begin
                SetError('Calling functions is not supported');
                Exit;
                end;
              Result := OnCallParamsFunc(FuncValue, ParamValues, FError);
              end;
            end
          else
            begin
            if not Assigned(OnGetIndexedValue) then
              begin
              SetError('Retrieving indexed values is not supported');
              Exit;
              end;
            Result := OnGetIndexedValue(FuncValue, ParamValues, FError);
            end;
        finally
          for i := 0 to High(ParamsExpr.Params) do
            ParamValues[i].Free;
        end;
      finally
        FuncValue.Free;
      end;
      end;
  else
    begin
    SetError('Unsupported expression (%s) in expression', [AnExpression.ClassName]);
    end;
  end;
end;

procedure TExpressionEvaluator.SetError(AnErrorMsg: string);
begin
  FError := AnErrorMsg;
end;

procedure TExpressionEvaluator.SetError(const AnErrorMsg: string; const Args: array of const);
begin
  SetError(Format(AnErrorMsg, Args));
end;

function TExpressionEvaluator.TryEvaluateBinaryArithmeticOperator(const ALeftValue, ARightValue: TEvaluationValue; const OpCode: TExprOpCode): TEvaluationValue;
type
  TTypeCastResultKind = (tcrInvalid, tcrUnsupported, tcrInteger, tcrString);
const
  TypeCastArr: array[TEvaluationValueKind, TEvaluationValueKind] of TTypeCastResultKind =
    (           {evkNone        evkString      evkBoolean     evkInteger     evkCardinal    evkFloat       evkFunction,   evkContainer}
{ evkNone     } (tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported),
{ evkString   } (tcrUnsupported,tcrString,     tcrUnsupported,tcrInvalid,    tcrInvalid,    tcrInvalid,    tcrInvalid,    tcrUnsupported,tcrUnsupported),
{ evkBoolean  } (tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrInvalid,    tcrUnsupported,tcrUnsupported),
{ evkInteger  } (tcrUnsupported,tcrInvalid,    tcrUnsupported,tcrInteger,    tcrInteger,    tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported),
{ evkCardinal } (tcrUnsupported,tcrInvalid,    tcrUnsupported,tcrInteger,    tcrInteger,    tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported),
{ evkFloat    } (tcrUnsupported,tcrInvalid,    tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrInvalid,    tcrUnsupported,tcrUnsupported),
{ evkFunction } (tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported),
{ evkContainer} (tcrUnsupported,tcrInvalid,    tcrInvalid    ,tcrInvalid,    tcrUnsupported,tcrInvalid,    tcrUnsupported,tcrUnsupported,tcrUnsupported),
{ evkType }     (tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported,tcrUnsupported)
    );
var
  TypeCastResultKind: TTypeCastResultKind;
  LeftValueInt, RightValueInt: Int64;
  RightValueString, LeftValueString: string;
  LeftKind, RightKind: TEvaluationValueKind;
  ErrMsg: string;
begin
  Result := nil;
  LeftKind := ALeftValue.Kind;
  RightKind := ARightValue.Kind;
  TypeCastResultKind := TypeCastArr[LeftKind, RightKind];
  case TypeCastResultKind of
    tcrInvalid:
      begin
      SetError('Incompatible types');
      Exit;
      end;
    tcrUnsupported:
      begin
      SetError('not-implemented typecase');
      Exit;
      end;
    tcrInteger:
      begin
      if not ALeftValue.TryGetAsInteger(LeftValueInt, ErrMsg) or
         not ARightValue.TryGetAsInteger(RightValueInt, ErrMsg) then
        begin
        SetError(ErrMsg);
        Exit;
        end;
      case OpCode of
        eopAdd:      LeftValueInt := LeftValueInt + RightValueInt;
        eopSubtract: LeftValueInt := LeftValueInt - RightValueInt;
        eopMultiply: LeftValueInt := LeftValueInt * RightValueInt;
        eopDiv:      LeftValueInt := LeftValueInt div RightValueInt;
      else
        begin
        SetError('Unsupported integer opcode');
        Exit;
        end;
      end;
      Result := TIntegerEvaluationValue.Create(LeftValueInt);
      end;
    tcrString:
      begin
      if not ALeftValue.TryGetAsString(LeftValueString, ErrMsg) or
         not ARightValue.TryGetAsString(RightValueString, ErrMsg) then
        begin
        SetError(ErrMsg);
        Exit;
        end;
      case OpCode of
        eopAdd:      LeftValueString := LeftValueString + RightValueString;
      else
        begin
        SetError('Unsupported string opcode');
        Exit;
        end;
      end;
      Result := TStringEvaluationValue.Create(LeftValueString);
      end;
  else
    // Should be dead code
    raise Exception.Create('Failed to evaluate binary operator');
  end;
end;

function TExpressionEvaluator.ExtractIdentifiers(AnExpression: TPasExpr): Boolean;
var
  AParamsExpr: TParamsExpr;
  i: Integer;
  IdentName: string;
begin
  Result := False;
  if not Assigned(AnExpression) then
    begin
    SetError('Failed to extract identifier: expression is missing');
    Exit;
    end;

  case AnExpression.Kind of
    pekBoolConst,
    pekNumber,
    pekString:
      begin
      // No identifier
      Result := True;
      end;
    pekBinary:
      begin
      if TBinaryExpr.IsRightSubIdent(TBinaryExpr(AnExpression).right) then
        Result := ExtractIdentifiers(TBinaryExpr(AnExpression).left)
      else
        Result := ExtractIdentifiers(TBinaryExpr(AnExpression).left) and ExtractIdentifiers(TBinaryExpr(AnExpression).right);
      end;
    pekIdent:
      begin
      IdentName := (AnExpression as TPrimitiveExpr).Value;
      if not assigned(FIdentifierList.FindByName(IdentName)) then
        FIdentifierList.Add(TEvaluationIdentifier.Create(IdentName));
      Result := True;
      end;
    pekUnary:
      begin
      Result := ExtractIdentifiers(TUnaryExpr(AnExpression).Operand);
      end;
    pekFuncParams,
    pekArrayParams:
      begin
      AParamsExpr := AnExpression as TParamsExpr;
      for i := 0 to High(AParamsExpr.Params) do
        begin
        Result := ExtractIdentifiers(AParamsExpr.Params[i]);
        if not Result then
          Exit;
        end;
      if Assigned(AParamsExpr.Value) then
        begin
        Result := ExtractIdentifiers(AParamsExpr.Value);
        if not Result then
          Exit;
        end;
      end;
  else
    SetError(Format('Unsupported expression (%s) in expression', [AnExpression.ClassName]));
  end;
end;

function TExpressionEvaluator.TryEvaluateUnaryOperator(const AnOperand: TEvaluationValue; const OpCode: TExprOpCode): TEvaluationValue;
var
  Err: string;
  Int: Int64;
  Bool: Boolean;
begin
  Result := nil;
  case OpCode of
    eopSubtract:
      begin
      case AnOperand.Kind of
        evkInteger:
          begin
          if AnOperand.TryGetAsInteger(Int, Err) then
            Result := TIntegerEvaluationValue.Create(-Int)
          else
            SetError(Err);
          end
      else
        SetError('Incompatible types');
      end;
      end;
    eopNot:
      begin
      case AnOperand.Kind of
        evkBoolean:
          begin
          if AnOperand.TryGetAsBoolean(Bool, Err) then
            Result := TBooleanEvaluationValue.Create(not Bool)
          else
            SetError(Err);
          end
      else
        SetError('Incompatible types');
      end;
      end;
  else
    SetError('Unsupported unary opcode');
  end;
end;

function TExpressionEvaluator.TryEvaluateBinaryLogicalOperator(const ALeftValue, ARightValue: TEvaluationValue; const OpCode: TExprOpCode): TEvaluationValue;
var
  LeftValueInt, RightValueInt: Int64;
  RightValueString, LeftValueString: string;
  RightValueBool, LeftValueBool: boolean;
  RightValueFloat, LeftValueFloat: ValReal;
  RightValueCardinal, LeftValueCardinal: QWord;
  ErrMsg: string;
  ResultBool: Boolean;
begin
  Result := nil;
  if ALeftValue.Kind <> ARightValue.Kind then
    begin
    SetError('Incompatible types');
    Exit;
    end;
  case ALeftValue.Kind of
    evkBoolean :
      begin
      if not ALeftValue.TryGetAsBoolean(LeftValueBool, ErrMsg) or
         not ARightValue.TryGetAsBoolean(RightValueBool, ErrMsg) then
        begin
        SetError(ErrMsg);
        Exit;
        end;
      case OpCode of
        eopEqual:            ResultBool := LeftValueBool = RightValueBool;
        eopNotEqual:         ResultBool := LeftValueBool <> RightValueBool;
        eopLessThan:         ResultBool := LeftValueBool < RightValueBool;
        eopLessthanEqual:    ResultBool := LeftValueBool <= RightValueBool;
        eopGreaterThan:      ResultBool := LeftValueBool > RightValueBool;
        eopGreaterThanEqual: ResultBool := LeftValueBool >= RightValueBool;
      else
        begin
        SetError('Unsupported logical boolean opcode');
        exit;
        end;
      end;
      end;
    evkInteger :
      begin
      if not ALeftValue.TryGetAsInteger(LeftValueInt, ErrMsg) or
         not ARightValue.TryGetAsInteger(RightValueInt, ErrMsg) then
        begin
        SetError(ErrMsg);
        Exit;
        end;
      case OpCode of
        eopEqual:            ResultBool := LeftValueInt = RightValueInt;
        eopNotEqual:         ResultBool := LeftValueInt <> RightValueInt;
        eopLessThan:         ResultBool := LeftValueInt < RightValueInt;
        eopLessthanEqual:    ResultBool := LeftValueInt <= RightValueInt;
        eopGreaterThan:      ResultBool := LeftValueInt > RightValueInt;
        eopGreaterThanEqual: ResultBool := LeftValueInt >= RightValueInt;
      else
        begin
        SetError('Unsupported logical boolean opcode');
        exit
        end;
      end;
      end;
    evkString :
      begin
      if not ALeftValue.TryGetAsString(LeftValueString, ErrMsg) or
         not ARightValue.TryGetAsString(RightValueString, ErrMsg) then
        begin
        SetError(ErrMsg);
        Exit;
        end;
      case OpCode of
        eopEqual:            ResultBool := LeftValueString = RightValueString;
        eopNotEqual:         ResultBool := LeftValueString <> RightValueString;
        eopLessThan:         ResultBool := LeftValueString < RightValueString;
        eopLessthanEqual:    ResultBool := LeftValueString <= RightValueString;
        eopGreaterThan:      ResultBool := LeftValueString > RightValueString;
        eopGreaterThanEqual: ResultBool := LeftValueString >= RightValueString;
      else
        begin
        SetError('Unsupported logical boolean opcode');
        exit
        end;
      end;
      end;
    evkFloat :
      begin
      if not ALeftValue.TryGetAsFloat(LeftValueFloat, ErrMsg) or
         not ARightValue.TryGetAsFloat(RightValueFloat, ErrMsg) then
        begin
        SetError(ErrMsg);
        Exit;
        end;
      case OpCode of
        eopEqual:            ResultBool := LeftValueFloat = RightValueFloat;
        eopNotEqual:         ResultBool := LeftValueFloat <> RightValueFloat;
        eopLessThan:         ResultBool := LeftValueFloat < RightValueFloat;
        eopLessthanEqual:    ResultBool := LeftValueFloat <= RightValueFloat;
        eopGreaterThan:      ResultBool := LeftValueFloat > RightValueFloat;
        eopGreaterThanEqual: ResultBool := LeftValueFloat >= RightValueFloat;
      else
        begin
        SetError('Unsupported logical boolean opcode');
        exit
        end;
      end;
      end;
    evkCardinal :
      begin
      if not ALeftValue.TryGetAsCardinal(LeftValueCardinal, ErrMsg) or
         not ARightValue.TryGetAsCardinal(RightValueCardinal, ErrMsg) then
        begin
        SetError(ErrMsg);
        Exit;
        end;
      case OpCode of
        eopEqual:            ResultBool := LeftValueCardinal = RightValueCardinal;
        eopNotEqual:         ResultBool := LeftValueCardinal <> RightValueCardinal;
        eopLessThan:         ResultBool := LeftValueCardinal < RightValueCardinal;
        eopLessthanEqual:    ResultBool := LeftValueCardinal <= RightValueCardinal;
        eopGreaterThan:      ResultBool := LeftValueCardinal > RightValueCardinal;
        eopGreaterThanEqual: ResultBool := LeftValueCardinal >= RightValueCardinal;
      else
        begin
        SetError('Unsupported logical boolean opcode');
        exit
        end;
      end;
      end;
  else
    begin
    SetError('Types can not be compared');
    exit
    end;
  end;
  Result := TBooleanEvaluationValue.Create(ResultBool);
end;

function TExpressionEvaluator.TryEvaluateSubIdentOperator(const ALeftValue: TEvaluationValue; const ARightExpression: TPasExpr): TEvaluationValue;
var
  Err: string;
begin
  Result := nil;
  if not (ARightExpression is TPrimitiveExpr) then
    begin
    SetError(ARightExpression.Name + ' is not a valid subidentifier');
    end
  else if not ALeftValue.TryObtainChild(TPrimitiveExpr(ARightExpression).Value, Result, Err) then
    begin
    SetError(Err);
    end
end;

end.

