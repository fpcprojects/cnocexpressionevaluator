{
   fpmake.pp for cnocExpressionEvaluator
}

{$ifndef ALLPACKAGES}
{$mode objfpc}{$H+}
program fpmake;

uses fpmkunit;
{$endif ALLPACKAGES}

procedure add_cnocExpressionEvaluator(const ADirectory: string);

var
  P : TPackage;
  T : TTarget;
  D : TDependency;

begin
  with Installer do
    begin
    P:=AddPackage('cnocExpressionEvaluator');
    P.Version:='0.6.1-1';

    P.Directory:=ADirectory;

    P.Author:='Joost van der Sluis (CNOC)';
    P.License:='LGPL v2  with static linking exception';
    P.Description:='Library to evaluate expressions as parsed by fcl-passrc';
    P.HomepageURL := 'https://gitlab.com/fpcprojects/cnocexpressionevaluator';

    D := P.Dependencies.Add('fcl-passrc');
    D := P.Dependencies.Add('rtl-generics');

    P.ExamplePath.Add('tests');
    P.Targets.AddExampleProgram('cnocexpressionevaluatorguitestrunner.pas');
    P.Sources.AddExample('tests/cnocexpressionevaluatorguitestrunner.lpi');
    P.Sources.AddExample('tests/expressiontests.pas');

    P.ExamplePath.Add('example');
    P.Targets.AddExampleProgram('expressionevaluatordemo.lpr');
    P.Sources.AddExample('example/evaluationform.pas');
    P.Sources.AddExample('example/evaluationform.lfm');
    P.Sources.AddExample('example/EvaluatorScreenshots.png');

    P.Sources.AddDoc('readme.md');
    P.Sources.AddDoc('changelog.md');
    P.Sources.AddDoc('COPYING.LGPL.txt');
    P.Sources.AddDoc('COPYING.modifiedLGPL.txt');
    P.Sources.AddDoc('example/EvaluatorScreenshots.png');
    T:=P.Targets.AddUnit('cnocexpressionevaluator.pas');
    end;
end;

{$ifndef ALLPACKAGES}
begin
  add_cnocExpressionEvaluator('');
  Installer.Run;
end.
{$endif ALLPACKAGES}
