### Changed
### Removed
### Added
### Fixed

## [0.6.1] - 2022-5-10
### Added
 - Support for typecasts

## [0.6.0] - 2021-5-14
### Added
 - Support for sub-identifiers and methods
 - Indexed array access
### Fixed
 - Solved AV after an invalid expression has been parsed
 - Solved problem with an uninitialized variable

## [0.5.1] - 2020-12-30
### Added
 - Ability to do calculations with cardinals and integers
 - Callbacks to be able to evaluate functions
### Fixed
 - Compatibility with fpc 3.2.0 and recent fpc 3.3.1
 - Fixed possible memory-corruption

## [0.5.0] - 2020-08-23
### Added
 - Initial version
