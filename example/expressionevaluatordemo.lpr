program expressionevaluatordemo;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  {$IFDEF HASAMIGA}
  athreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  EvaluationForm;

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Title := 'ExpressionEvaluatorDemo';
  Application.Scaled := True;
  Application.Initialize;
  Application.CreateForm(TfEvaluationForm, fEvaluationForm);
  Application.Run;
end.

