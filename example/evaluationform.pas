unit EvaluationForm;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Forms,
  Controls,
  Graphics,
  Dialogs,
  ExtCtrls,
  StdCtrls,
  ActnList,
  cnocExpressionEvaluator;

type

  { TfEvaluationForm }

  TfEvaluationForm = class(TForm)
    pHeader: TPanel;
    lHeader: TLabel;
    pExpression: TPanel;
    lExpression: TLabel;
    eExpression: TEdit;
    lResult: TLabel;
    eResult: TEdit;
    bEvaluate: TButton;
    ActionList: TActionList;
    aEvaluate: TAction;
    procedure aEvaluateExecute(Sender: TObject);
  end;

var
  fEvaluationForm: TfEvaluationForm;

implementation

{$R *.lfm}

{ TfEvaluationForm }

procedure TfEvaluationForm.aEvaluateExecute(Sender: TObject);
var
  Evaluator: TExpressionEvaluator;
  Value: TEvaluationValue;
begin
  Evaluator := TExpressionEvaluator.Create(eExpression.Text);
  try
    if not Evaluator.Parse then
      begin
      eResult.Text := 'Error: ' + Evaluator.Error;
      eResult.Font.Color := clRed;
      end
    else
      begin
      Value := Evaluator.Evaluate;
      try
        if not Assigned(Value) then
          begin
          eResult.Text := 'Error: ' + Evaluator.Error;
          eResult.Font.Color := clRed;
          end
        else
          begin
          eResult.Text := Value.AsExpressionString;
          eResult.Font.Color := clDefault;
          end;
      finally
        Value.Free;
      end;
      end;
  finally
    Evaluator.Free;
  end;
end;

end.

